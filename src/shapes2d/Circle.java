package shapes2d;

import shapes.TwoDimensionalShape;

public class Circle extends TwoDimensionalShape
{
    double radius;

    public Circle(double r)
    {
        radius = r;
    }

    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return Math.PI * radius * radius;
    }
    public double getCircumference()
    {
        return 2 * Math.PI * radius;
    }
    public double getDiameter()
    {
        return radius * 2;
    }

    @Override
    public String toString()
    {
        return "{Circle Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", radius= " + this.getRadius() // Yarıçap
                + ", circumference= " + this.getCircumference() // Çevre
                + ", area= " + this.getArea()  // Alan
                + ", diameter= " + this.getDiameter() // Çap
                + "}";
    }
}
