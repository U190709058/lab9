package shapes2d;

import shapes.TwoDimensionalShape;

public class Square extends TwoDimensionalShape
{
    double size;

    public Square(double s)
    {
        size = s;
    }

    public double getSize()
    {
        return size;
    }
    public double getArea()
    {
        return size * size;
    }
    public double getCircumference()
    {
        return 4 * size;
    }

    @Override
    public String toString()
    {
        return "{Square Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", size= " + this.getSize() // kenar
                + ", circumference= " + this.getCircumference() // çevre
                + ", area= " + this.getArea() // alan
                + "}";
    }
}
