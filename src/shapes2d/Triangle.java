package shapes2d;

import shapes.TwoDimensionalShape;

import java.util.Arrays;

public class Triangle extends TwoDimensionalShape
{
    double side1;
    double side2;
    double side3;
    double height;

    public Triangle(double s1, double s2, double s3, double h)
    {
        side1 = s1;
        side2 = s2;
        side3 = s3;
        height = h;
    }

    // Height is NOT necessary for this shape:
    // Çevre = a+b+c
    // Alan = √(s(s-a)(s-b)(s-c)), s = Çevre/2

    public double[] getSides()
    {
        return new double[]{this.side1, this.side2, this.side3};
    }
    public double getHeight()
    {
        return height;
    }
    public double getCircumference()
    {
        return side1 + side2 + side3;
    }
    public double getArea()
    {
        return Math.sqrt((this.getCircumference()/2) * ((this.getCircumference()/2)-side1) * ((this.getCircumference()/2)-side2) * ((this.getCircumference()/2)-side3));
    }

    @Override
    public String toString()
    {
        return "{Triangle Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", sides= " + Arrays.toString(this.getSides()) // Kenarlar
                + ", height= " + this.getHeight() // Yükseklik
                + ", circumference= " + this.getCircumference() // Çevre
                + ", area= " + this.getArea()  // Alan
                + "}";
    }
}
