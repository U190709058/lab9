import shapes.Shape;
import shapes2d.Circle;
import shapes2d.Square;
import shapes2d.Triangle;
import shapes3d.Cube;
import shapes3d.Sphere;
import shapes3d.Tetrahedron;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Tester {
    public static void main(String[] args) {

        ArrayList<Shape> shapes = new ArrayList<>();
        double totalarea = 0, totalperimeter = 0, totalvolume = 0;

        try {
            File myFile = new File("instructor.txt");
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] splitted = data.split(" ");

                switch (splitted[0]) {
                    case "O":
                        switch (splitted[1]) {
                            case "C" -> {
                                Circle circ = new Circle(Double.parseDouble(splitted[2]));
                                System.out.println(circ);
                                shapes.add(circ);
                                totalperimeter += circ.getCircumference();
                                totalarea += circ.getArea();
                            }
                            case "S" -> {
                                Square squ = new Square(1.9);
                                System.out.println(squ);
                                shapes.add(squ);
                                totalperimeter += squ.getCircumference();
                                totalarea += squ.getArea();
                            }
                            case "T" -> {
                                Triangle tri = new Triangle(Double.parseDouble(splitted[2]), Double.parseDouble(splitted[3]), Double.parseDouble(splitted[4]), Double.parseDouble(splitted[5]));
                                System.out.println(tri);
                                shapes.add(tri);
                                totalperimeter += tri.getCircumference();
                                totalarea += tri.getArea();
                            } case "SP" -> {
                                Sphere sphere = new Sphere(Double.parseDouble(splitted[2]));
                                System.out.println(sphere);
                                shapes.add(sphere);
                                totalarea += sphere.getArea();
                                totalvolume += sphere.getVolume();
                            }
                            case "CU" -> {
                                Cube cube = new Cube(Double.parseDouble(splitted[2]));
                                System.out.println(cube);
                                shapes.add(cube);
                                totalarea += cube.getArea();
                                totalvolume += cube.getVolume();
                            }
                            case "TE" -> {
                                Tetrahedron tetra = new Tetrahedron(Double.parseDouble(splitted[2]));
                                System.out.println(tetra);
                                shapes.add(tetra);
                                totalarea += tetra.getArea();
                                totalvolume += tetra.getVolume();
                            }
                        }
                        break;
                    case "TA":
                        System.out.println(totalarea);
                        break;
                    case "TP":
                        System.out.println(totalperimeter);
                        break;
                    case "TV":
                        System.out.println(totalvolume);
                        break;
                    case "SL":
                        System.out.println(shapes);
                        break;
                }

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
