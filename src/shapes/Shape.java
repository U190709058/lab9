package shapes;

public class Shape
{
    static int UNIQUE_ID = 0;
    final int uid = ++UNIQUE_ID;

    public int getUid()
    {
        return uid;
    }

    public static void main(String[] args)
    {
        Shape myObj = new Shape();
        System.out.println(myObj.getUid());
    }
}
