package shapes3d;

import shapes.ThreeDimensionalShape;
import shapes2d.Triangle;

public class Tetrahedron extends ThreeDimensionalShape
{
    Triangle triangle;
    double side;

    public Tetrahedron(double s)
    {
        side = s;
        this.triangle = new Triangle(s, s, s, 0);
    }

    public double getSide()
    {
        return this.side;
    }
    public Triangle getTriangle()
    {
        return triangle;
    }
    /*public double getCircumference()
    {
        return 6 * this.side;
    }*/
    public double getArea()
    {
        return 4 * triangle.getArea();
    }
    public double getVolume()
    {
        return (this.side * this.side * this.side * Math.sqrt(2)) / 12 ;
    }

    @Override
    public String toString()
    {
        return "{Tetrahedron Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", side= " + this.getSide() // kenar
                + ", insidetriangle= " + this.getTriangle() // yüzey üçgen
//                + ", circumference= " + this.getCircumference() // Çevre
                + ", area= " + this.getArea() // Alan
                + ", volume= " + this.getVolume() // Hacim
                + "}";
    }
}
