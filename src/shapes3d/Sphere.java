package shapes3d;

import shapes.ThreeDimensionalShape;
import shapes2d.Circle;

public class Sphere extends ThreeDimensionalShape
{
    Circle circle;

    public Sphere(double r) {
        this.circle = new Circle(r);
    }

    public double getRadius()
    {
        return circle.getRadius();
    }
    public Circle getCircle()
    {
        return circle;
    }
    public double getArea()
    {
        return 4 * circle.getArea();
    }
    public double getVolume()
    {
        return 4.0 / 3.0 * Math.PI * circle.getRadius() * circle.getRadius() * circle.getRadius();
    }

    @Override
    public String toString()
    {
        return "{Sphere Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", radius= " + this.getRadius() // yarıçap
                + ", insidecircle= " + this.getCircle() // İç daire
                + ", area= " + this.getArea() // Alan
                + ", volume= " + this.getVolume() // Hacim
                + "}";
    }
}