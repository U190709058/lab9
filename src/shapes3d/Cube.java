package shapes3d;

import shapes.ThreeDimensionalShape;
import shapes2d.Square;

public class Cube extends ThreeDimensionalShape
{
    Square square;

    public Cube(double s) {
        this.square = new Square(s);
    }

    public double getSize()
    {
        return square.getSize();
    }
    public Square getSquare()
    {
        return square;
    }
    /*public double getCircumference()
    {
        return 12 * square.getSize();
    }*/
    public double getArea()
    {
        return 6 * square.getArea();
    }
    public double getVolume()
    {
        return square.getArea() * square.getSize();
    }

    @Override
    public String toString()
    {
        return "{Cube Object: " +
                "uid= " + this.getUid()  // ID
                + ", dimension= " + this.getDim() // 2d
                + ", size= " + this.getSize() // kenar
                + ", insidesquare= " + this.getSquare() // İç Kareleri
//                + ", circumference= " + this.getCircumference() // Çevre
                + ", area= " + this.getArea() // Alan
                + ", volume= " + this.getVolume() // Hacim
                + "}";
    }
}